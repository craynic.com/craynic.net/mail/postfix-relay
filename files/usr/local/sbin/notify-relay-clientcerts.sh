#!/usr/bin/env bash

set -Eeuo pipefail

FINGERPRINTS_FILE="/opt/postfix/relay-clientcerts/fingerprints"

inotifywait-dir.sh "$FINGERPRINTS_FILE" | while read -r line; do
  echo "$line"

  # re-hash the file
  /usr/local/sbin/postmap-relay-clientcerts.sh
done
