#!/usr/bin/env bash

set -Eeuo pipefail

FINGERPRINT_FILE="/opt/postfix/relay-clientcerts/fingerprints"

if [[ ! -f "$FINGERPRINT_FILE" ]]; then
  echo "File with fingerprints of the client certificates does not exist!" >/dev/stderr
  exit 1
fi

postmap "$FINGERPRINT_FILE"
